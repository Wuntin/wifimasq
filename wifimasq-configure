#!/bin/bash

set -e
set -o pipefail

default_primary_nic() {
    local nic=$(ip --oneline link | cut -f 2 -d ' ' | grep '^w' | sort | head -1)
    nic=${nic%:}

    echo "${nic}"
}

default_secondary_nic() {
    local primary_nic=$1
    local nic=$(ip --oneline link | cut -f 2 -d ' ' | grep '^w' | grep -v "^${primary_nic}:$" | sort | head -1)
    nic=${nic%:}

    echo "${nic}"
}

update_config_file() {
    local filename=$1
    local new_content=$2
    local current_content=""

    if [ -n "${WIFIMASQ_DEBUG}" ]
    then
        echo "${filename}:"
        echo "${new_content}"
        echo ""

        return 0
    fi

    if [ -e "${filename}" ]
    then
	      current_content=$(cat "${filename}")
    fi

    if [ "${new_content}" == "${current_content}" ]
    then
	      echo "No changes made to ${filename}."
	      return 0
    fi

    echo "Updating ${filename}."
    echo "${new_content}" > "${filename}"

    return 1
}

write_primary_nic_config() {
    local content=$(cat <<-EOF
allow-hotplug ${PRIMARY_NIC}
iface ${PRIMARY_NIC} inet dhcp
  wpa-conf /etc/wpa_supplicant/wpa_supplicant-uplink.conf
EOF
	  )

    if ! update_config_file "/etc/network/interfaces.d/${PRIMARY_NIC}" "${content}"
    then
        RESTART_PRIMARY_NIC=1
    fi
}

write_secondary_nic_config() {
    local content=$(cat <<-EOF
allow-hotplug ${SECONDARY_NIC}
iface ${SECONDARY_NIC} inet static
  address 172.31.200.1/24
  wpa-conf /etc/wpa_supplicant/wpa_supplicant-local.conf
EOF
	  )

    if ! update_config_file "/etc/network/interfaces.d/${SECONDARY_NIC}" "${content}"
    then
        RESTART_SECONDARY_NIC=1
    fi
}

write_primary_wpa_supplicant_config() {
    local content=$(cat <<-EOF
${WPA_SUPPLICANT_HEADER}

$(wpa_passphrase "${UPLINK_SSID}" "${UPLINK_PASSPHRASE}")
EOF
    )

    if ! update_config_file "/etc/wpa_supplicant/wpa_supplicant-uplink.conf" "${content}"
    then
        RESTART_PRIMARY_NIC=1
    fi
}

write_secondary_wpa_supplicant_config() {
    local content=$(cat <<-EOF
${WPA_SUPPLICANT_HEADER}

$(wpa_passphrase "${LOCAL_SSID}" "${LOCAL_PASSPHRASE}" | sed -e '/^}$/ i \\tmode=2\n\tkey_mgmt=WPA-PSK\n\tpairwise=CCMP TKIP\n\tgroup=CCMP TKIP')
EOF
    )

    if ! update_config_file "/etc/wpa_supplicant/wpa_supplicant-local.conf" "${content}"
    then
        RESTART_SECONDARY_NIC=1
    fi
}

write_nftables_config() {
    local content=$(cat <<-EOF
#!/usr/bin/nft -f

define wan_if = ${PRIMARY_NIC}
define lan_if = ${SECONDARY_NIC}

flush ruleset

table ip nat {
  chain postrouting {
    type nat hook postrouting priority 100;
    policy accept;

    oifname \$wan_if counter masquerade
  }
}

table ip6 nat {
  chain postrouting {
    type nat hook postrouting priority 100;
    policy accept;

    oifname \$wan_if counter masquerade
  }
}

table inet filter {
  chain input {
    type filter hook input priority filter;
    policy drop;

    ct state established,related counter accept
    iifname "lo" ct state new counter accept
    iifname \$lan_if ct state new counter accept
    ct state new tcp dport ssh counter accept
    limit rate 3/hour counter log prefix "INPUT - "
  }
  chain forward {
    type filter hook forward priority filter;
    policy drop;

    ct state established,related counter accept
    iifname \$lan_if oifname \$wan_if ct state new counter accept
    iifname \$wan_if oifname \$lan_if ct state new counter accept
    limit rate 3/hour counter log prefix "FORWARD - "
  }
  chain output {
    type filter hook output priority filter;
    policy drop;

    ct state established,related counter accept
    oifname "lo" ct state new counter accept
    oifname \$lan_if ct state new counter accept
    oifname \$wan_if ct state new counter accept
    limit rate 3/hour counter log prefix "OUTPUT - "
  }
}
EOF
    )

    if ! update_config_file "/etc/nftables.conf" "${content}"
    then
	RESTART_NFTABLES=1
    fi
}

write_dhcpd_config() {
    local content=$(cat <<-EOF
default-lease-time 1800;
max-lease-time 3600;
use-host-decl-names on;

option netbios-node-type 8;

subnet 172.31.200.0 netmask 255.255.255.0 {
  authoritative;

  option broadcast-address 172.31.200.255;
  option routers 172.31.200.1;
  option domain-name-servers 172.31.200.1;
  option ntp-servers 172.31.200.1;

  range 172.31.200.200 172.31.200.250;
}
EOF
    )

    if ! update_config_file "/etc/dhcp/dhcpd.conf" "${content}"
    then
        RESTART_DHCPD=1
    fi
}

restart_primary_nic() {
    if [ -n "${RESTART_PRIMARY_NIC}" ]
    then
        echo -n "Restarting primary NIC... "
        ifdown ${PRIMARY_NIC} > /dev/null 2>&1
        ifup ${PRIMARY_NIC} > /dev/null 2>&1
        echo "done."
    fi
}

restart_secondary_nic() {
    if [ -n "${RESTART_SECONDARY_NIC}" ]
    then
        echo -n "Restarting secondary NIC... "
        ifdown ${SECONDARY_NIC} > /dev/null 2>&1
        ifup ${SECONDARY_NIC} > /dev/null 2>&1
        echo "done."
    fi
}

restart_nftables() {
    if [ -n "${RESTART_NFTABLES}" ]
    then
	echo -n "Reloading nftables rules... "
	systemctl restart nftables > /dev/null 2>&1
	echo "done."
    fi
}

restart_dhcpd() {
    if [ -n "${RESTART_DHCPD}" ]
    then
        echo -n "Restarting DHCPD server... "
        systemctl restart isc-dhcp-server > /dev/null 2>&1
        echo "done."
    fi
}

WPA_SUPPLICANT_HEADER=$'ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev\nupdate_config=1'

PRIMARY_NIC=${PRIMARY_NIC:-$(default_primary_nic)}
SECONDARY_NIC=${SECONDARY_NIC:-$(default_secondary_nic ${PRIMARY_NIC})}
UPLINK_SSID=${UPLINK_SSID:-uplink}
UPLINK_PASSPHRASE=${UPLINK_PASSPHRASE:-password}
LOCAL_SSID=${LOCAL_SSID:-local}
LOCAL_PASSPHRASE=${LOCAL_PASSPHRASE:-password}

write_primary_nic_config
write_secondary_nic_config
write_primary_wpa_supplicant_config
write_secondary_wpa_supplicant_config
write_nftables_config
write_dhcpd_config
#write_bind_config
#write_resolvconf_config
restart_primary_nic
restart_secondary_nic
restart_nftables
restart_dhcpd
